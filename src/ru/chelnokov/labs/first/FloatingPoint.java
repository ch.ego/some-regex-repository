package ru.chelnokov.labs.first;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FloatingPoint {
    public static void main(String[] args) {
        //допустим, числа разделены запятыми или пробелами
        String maybeFloats = "23.12e+10 и 28.12E+10, но не 135 и не 347.536";
        String regEx = "[-+]?[0-9]*\\.?[0-9]+([e][-+]?[0-9]+)";
        Pattern pattern = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(maybeFloats);
        matcher.results().forEach(result -> System.out.println(result.group()));
    }
}
